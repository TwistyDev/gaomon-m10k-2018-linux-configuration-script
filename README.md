## Gaomon M10K 2018 linux configuration script

Made by A. Pinsard

This is a small program to configure my drawing tablet "Gaomon M10K 2018" on Linux (tested on Manjaro KDE version and Pop_os!), because there's no official one.  
It can probably be easily adapted to be used with other drawing tablets...  
I make it public in case someone is in the same needs as me, but it is made for mines. (the code/method might be bad but I don't need to make it better right now)  
That's why it might not be super user friendly, but if you just want to use the buttons of your tablet it should be enough.  

/!\ This program isn't a driver, in my case the drawing tablet works out of the box with my system, I just need to change the active screen and the buttons' action.  
/!\ I have no link at all with Gaomon, this is (obviously) completely unofficial/!\  

#### Features :
- Configuring the 11 buttons of the tablet to be keyboard shortcuts.
- Changing the active screen of the tablet (useful if you are in dual screen configuration like me)
- Reverse the tablet for left-handed configuration.
- Creates a little tray icon to control basics functionalities.

#### Idea to do :
- Enabling the wheel (to be scroll), not worth the effort for me
- Little gui for ease of use, yay !

#### Dependancies :
- You need xinput to be installed and in the path
- inputs (https://pypi.org/project/inputs/)
- The user must be in the input group

#### How to use ? :
Configure the mapping by modifying the dictionary bellow (add a list if you want to do an input).
BTN_0 is the top button and BTN_SOUTH the bottom one.
Don't change BTN_EAST (it is part of the wheel system) 
Just launch this script.